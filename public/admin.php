<?php

include 'pdoConfig.php';

try {
    $db = new PDO($dsn, $username, $dbPassword);
} catch (PDOException $error) {
    echo 'Connection error:' . $error->getMessage();
}

if(empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW']))
{
    Header("WWW-Authenticate: Basic realm=\"Admin Page\"");
    Header("HTTP/1.0 401 Unauthorized");
    print('<h1>401 Требуется авторизация</h1>');
    ?>
    <a href="admin.php">АВТОРИЗОВАТЬСЯ</a>
    <?php
    exit();
}else {
    try {
        $stmt = $db->prepare("SELECT admin_password FROM admins WHERE admin_login = :ulogin LIMIT 1");
        $stmt->bindParam(':ulogin', $_SERVER['PHP_AUTH_USER']);
        $stmt->execute();

        $result = $stmt->fetch();
    } catch (PDOException $e) {
        print('PDOError : ' . $e->getMessage());
        exit();
    }

    if ($result == NULL) {
        echo 'Такого пользователя нет!';
        Header("WWW-Authenticate: Basic realm=\"Admin Page\"");
        Header("HTTP/1.0 401 Unauthorized");
        exit();
    }

    if (md5($_SERVER['PHP_AUTH_PW']) != $result['admin_password']) {
        Header("WWW-Authenticate: Basic realm=\"Admin Page\"");
        Header("HTTP/1.0 401 Unauthorized");
        exit();
    } else {
        echo '<div style="font-size: 175%; margin:1%"><b>Вы успешно авторизовались и видите защищенные паролем данные</b></div>';
    }


    try {
        $stmt = $db->prepare("SELECT COUNT(*) FROM person");
        $stmt->execute();
        $countOfPerson = $stmt->fetch();

        $stmt =  $db->prepare("SELECT * FROM superpowers ");
        $stmt->execute();
        $super = $stmt->fetchAll();

        $stmt = $db->prepare("SELECT * FROM person");
        $stmt->execute();
        $sql = $stmt->fetchAll();

    } catch (PDOException $e) {
        exit();
    }


    $prev=0;
    $h=-1;
    $mop = array();
    foreach($super as $real)
    {
        if ($real[0]==$prev)
        {
            $prev = $real[0];
            $mop[$h]=$mop[$h].", ".$real[1];
        }else {
            $h++;
            $prev=$real[0];
            $mop[$h]=$real[1];
        }
    }

    $i=0;
    if ($sql) {
        echo "<table id='myTable' class='table_dark'><tr><th>Id</th><th>Имя</th><th>Email</th><th>Дата рождения</th><th>Пол</th><th>Количество конечностей</th><th>О человеке</th><th>Суперспособность</th><th>Удалить</th></tr>";
            foreach($sql as $row) {
                echo "<tr>";
                for ($j = 0 ; $j < 9 ; ++$j)
                    if ($j == 8) {
                        echo "<td><form action='delete.php?$row[0]' method='POST'><button>Удалить</button></form></td>";
                    } else if ($j==7){
                        echo "<td><a href='update.php' class='clickable'>$mop[$i]</a></td>";
                        $i++;
                    } else {
                        echo "<td><a href='update.php' class='clickable'>$row[$j]</a></td>";
                    }
                echo "</tr>";
            }
        echo "</table>";
    }
}
include('adminStyle.html');
?>




