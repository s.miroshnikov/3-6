<?php

include 'pdoConfig.php';

if(empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW']))
{
    Header("WWW-Authenticate: Basic realm=\"Admin Page\"");
    Header("HTTP/1.0 401 Unauthorized");
    print('<h1>401 Требуется авторизация</h1>');
    ?>
    <a href="admin.php">АВТОРИЗОВАТЬСЯ</a>
    <?php
    exit();
}else {
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        try {
            $db = new PDO($dsn, $username, $dbPassword);
        } catch (PDOException $error) {
            echo 'Connection error:' . $error->getMessage();
        }

        $url = ($_SERVER['REQUEST_URI']);
        $id = parse_url($url, PHP_URL_QUERY);

        try {
            $stmt = $db->prepare("DELETE FROM person WHERE person.id = :unique");
            $stmt->bindParam(':unique', $id);
            $stmt->execute();

            $stmt = $db->prepare("DELETE FROM superpowers WHERE person_id = :unique");
            $stmt->bindParam(':unique', $id);
            $stmt->execute();
        } catch (PDOException $e) {
            print('PDOError : ' . $e->getMessage());
            exit();
        }
    }
}
?>

<div>
<p style="font-size: larger; font-weight: bolder">Удалено</p>
<a href="admin.php">назад</a>
</div>

<style>
*{
    margin: 0;
    padding: 0;
}

html{
    background-color: #ffffff;
    font-family:Roboto Light, sans-serif;
    width: 100%;
    height: 100%;
    text-align: center;
    font-weight: bold;
    font-size: larger;
}

a{
    text-decoration: none;
    color: #000000;
}

a:hover{
    color: #5a5a5a;
}

</style>