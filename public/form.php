<?php
if (!empty($messages)) {
    print('<div id="messages">');
    foreach ($messages as $message) {
        print($message);
    }
    print('</div>');
}

?>

<html lang="ru">
<body>
<div style="text-align: center">
    <a id="admin" href="admin.php">ADMIN</a>
    <br>
    <a id="login" href="login.php">login</a>
</div>

<form name="homework" id="form" action="index.php" method="POST" >
    <hr style="">
    <h4 style="font-size:16pt; font-family: Roboto, sans-serif">Заполните данные</h4>
    <label><b>Введите ваше имя:</b><br>
        <input class="text-border" name ="name"<?php if ($errors['name']) {print 'class="error"';} ?> value="<?php print $values['name']; ?>"  required pattern="^[А-Яа-яЁё\s]+$|^[a-zA-Z]+$" type="text" placeholder="Дмитрий" ></label>
    <br><br>
    <label><b>Введите адрес электронной почты</b><br>
        <input type="email" name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email'];?>" class="text-border" required pattern=".+\.com|.+\.ru" placeholder="dmitii.miroshnikov@gmail.com"></label>
    <br><br>
    <label><b>Введите дату рождения</b><br>
        <input type="date" name="dateOfBirth" <?php if ($errors['dateOfBirth']) {print 'class="error"';} ?> value="<?php print $values['dateOfBirth'];?>" class="text-border" required></label>
    <br><br>
    <b>Ваш пол</b>
    <br>
    <label> <input type="radio" name="first_radio_group" value="Мужской" <?php if($values['sop']=='Мужской')echo ' checked' ?> required>Мужской</label>
    <label> <input type="radio" name="first_radio_group" value="Женский" <?php if($values['sop']=='Женский')echo ' checked' ?>>Женский</label>
    <br><br>
    <b>Сколько у вас конечностей?</b>
    <br>
    <label><input type="radio" name="second_radio_group" value="Меньше четырёх" <?php if($values['col']=='Меньше четырёх')echo ' checked' ?> required>Меньше 4</label>
    <label><input type="radio" name="second_radio_group" value="Четыре" <?php if($values['col']=='Четыре')echo ' checked' ?>>4</label>
    <label><input type="radio" name="second_radio_group" value="Больше четырёх" <?php if($values['col']=='Больше четырёх')echo ' checked' ?>>Больше 4</label>
    <br><br>
    <label><b>Какую сверхспособность вы выберете?</b>
        <br>
        <select name="superpower[]" multiple="multiple" class="list-box" required>
            <option value="Бессмертие" <?php if($valuesOfSP[0]=='Бессмертие')echo ' selected' ?>>Бессмертие</option>
            <option value="Прохождение сквозь стены" <?php if($valuesOfSP[1]=='Прохождение сквозь стены')echo ' selected' ?>>Прохождение сквозь стены</option>
            <option value="Левитация" <?php if($valuesOfSP[2]=='Левитация')echo ' selected' ?>>Левитация</option>
        </select>
    </label>
    <br><br>
    <label><b>Расскажите о себе:</b>
        <br>
        <textarea name="message" maxlength="256" id="text-box" placeholder="Я родился в..."></textarea>
    </label>
    <br><br>
    <label><b>Я  ознакомлен с контрактом</b>
        <input type="checkbox" name="contract" required>
    </label>
    <br><br>
    <label>
        <input type="submit" value="Отправить"><br>
    </label>
</form>

<br><br><a style="text-align: center" class="exit" href="login.php?do=logout">Выход</a>

</body>
</html>
<style>
    *{
        margin: 0;
        padding: 0;
    }

    html{
        background-color: #ffffff;
        font-family:Roboto Light, sans-serif;
        width: 100%;
        height: 100%;
    }

body{
    text-align: center;
}

    .exit{
        font-size: 150%;
        text-decoration: none;
        font-weight: bolder;
        color:black;
    }

    .exit:hover{
        color: #646464;
    }

    html{
        background-color: #ffffff;
        font-family:Roboto Light, sans-serif;
    }

    header{
        background-color: #000000;
        padding: 10px;
        height: 100px;
        border-bottom-left-radius: 10px;
        border-bottom-right-radius: 10px;
    }

    ol{
        list-style-position: inside
    }

    .text-border{
        border:1px solid #AAA;
        border-radius: 2px;
        width: 240px;
        height: 22px;
        color: #161616;
        font: 400 13px Arial;
    }
    .text-border:focus{
        box-shadow: 0 1px 2px #626262 inset;
        border-color: #4285f4;
        outline: none;
    }

    .list-box{
        border:1px solid #AAA;
        border-radius: 2px;
        width: 240px;
        height: 50px;
        color: #161616;
        font: 400 13px Arial;
    }
    .list-box:focus{
        border-color: #4285f4;
        outline: none;
    }

    #text-box{
        border:1px solid #AAA;
        border-radius: 2px;
        width: 240px;
        height: 60px;
        color: #161616;
        font: 400 13px Arial;
    }
    #text-box:focus{
        border-color: #4285f4;
        outline: none;
    }

    #form{
        line-height: 25px;
        margin: 5%;
    }

    table {
        border-collapse: collapse;
        border: 3px solid black;
    }
    td {
        padding: 2px 15px;
        border: 2px solid black;
        color: #669;
    }
    th{
        border: 2px solid black;
        padding: 2px 15px;
        color: #44446b;
    }
    tr:nth-child(2n) {
        background: #e8edff;
    }

    hr{
        border: none;
        background-color: #000000;
        color:#000000;
        height: 0.5cm;
        width: 100%;
        text-align: center;
    }

    #admin{
        text-decoration: none;
        font-weight: bolder;
        color: #000000;
        font-size: 250%;
        cursor: pointer;
    }

    #admin:hover{
        color: #737373;
    }

    #login{
        text-decoration: none;
        font-weight: bolder;
        color: #000000;
        font-size: 250%;
        cursor: pointer;
        margin-bottom: -10px;
    }

    #login:hover{
        color: #737373;
    }
</style>