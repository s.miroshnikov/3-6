<?php

include 'pdoConfig.php';

header('Content-Type: text/html; charset=UTF-8');

try {
    $db = new PDO($dsn, $username, $dbPassword);
} catch (PDOException $error) {
    echo 'Connection error:' . $error->getMessage();
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();

    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('pass', '', 100000);
        $messages[] = 'Спасибо, результаты сохранены.';
    }

    if (!empty($_COOKIE['pass'])) {
        $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
            strip_tags($_COOKIE['login']),
            strip_tags($_COOKIE['pass']));
    }

    $errors = array();
    $errors['name'] = !empty($_COOKIE['name_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['dateOfBirth'] = !empty($_COOKIE['dateOfBirth_error']);
    $errors['sop'] = !empty($_COOKIE['sop_error']);
    $errors['col'] = !empty($_COOKIE['col_error']);
    $errors['superpower'] = !empty($_COOKIE['superpower_error']);
    $errors['aboutperson'] = !empty($_COOKIE['aboutperson_error']);
    $errors['contract'] = !empty($_COOKIE['contract_error']);

    if (!empty($errors['name'])) {
        setcookie('name_error', '', 100000);
        $messages[] = '<div class="error">Заполните имя.</div>';
    }
    if (!empty($errors['email'])) {
        setcookie('email_error', '', 100000);
        $messages[] = '<div class="error">Заполните email.</div>';
    }
    if (!empty($errors['dateOfBirth'])) {
        setcookie('dateOfBirth_error', '', 100000);
        $messages[] = '<div class="error">Заполните дату рождения.</div>';
    }
    if (!empty($errors['sop'])) {
        setcookie('sop_error', '', 100000);
        $messages[] = '<div class="error">Проблемы с вашим полом</div>';
    }
    if (!empty($errors['col'])) {
        setcookie('col_error', '', 100000);
        $messages[] = '<div class="error">Проблемы с количеством конечностей</div>';
    }
    if (!empty($errors['aboutperson'])) {
        setcookie('aboutperson_error', '', 100000);
        $messages[] = '<div class="error">Проблемы с вашим описанием</div>';
    }
    if (!empty($errors['superpower'])) {
        setcookie('contract_error', '', 100000);
        $messages[] = '<div class="error">Проблемы со сверхспособностью</div>';
    }
    if (!empty($errors['contract'])) {
        setcookie('contract_error', '', 100000);
        $messages[] = '<div class="error">Вы должны ознакомиться с контрактом.</div>';
    }

    $valuesOfSP = array();
    $values = array();
    $values['name'] = empty($_COOKIE['name_value']) ? '' : strip_tags($_COOKIE['name_value']);
    $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
    $values['dateOfBirth'] = empty($_COOKIE['dateOfBirth_value']) ? '' : strip_tags($_COOKIE['dateOfBirth_value']);
    $values['sop'] = empty($_COOKIE['sop_value']) ? '' : strip_tags($_COOKIE['sop_value']);
    $values['col'] = empty($_COOKIE['col_value']) ? '' : strip_tags($_COOKIE['col_value']);
    $valuesOfSP[0] = '';
    $valuesOfSP[1] = '';
    $valuesOfSP[2] = '';

    if (isset($_COOKIE['superpower_value'])) {
        foreach ($_COOKIE['superpower_value'] as $name => $value) {
            $name = htmlspecialchars($name);
            $value = htmlspecialchars($value);
            if(!empty($value))$valuesOfSP[$name] = strip_tags($value);
        }
    }


    if (session_start() && !empty($_SESSION['login']) && !empty($_COOKIE[session_name()])) {
        $stmt = $db->prepare("SELECT * FROM person WHERE person.id = (SELECT user_id FROM users WHERE user_login = :login)");
        $stmt->BindParam(":login", $_SESSION['login']);
        $stmt->execute();

        $sql = $stmt->fetch();

        $stmt = $db->prepare("SELECT superpower FROM superpowers WHERE person_id = :login");
        $stmt->bindParam(':login', $_SESSION['login']);
        $stmt->execute();

        $sup = $stmt->fetchAll();

        $values['name'] = strip_tags($sql['name']);
        $values['email'] = strip_tags($sql['email']);
        $values['dateOfBirth'] = strip_tags($sql['dateofbirth']);
        $values['sop'] = strip_tags($sql['sop']);
        $values['col'] = strip_tags($sql['col']);

        foreach ($sup as $name => $s) {
            $name = htmlspecialchars($name);
            $value = htmlspecialchars($s);
            if (!empty($value)) $valuesOfSP[$name] = strip_tags($value);
        }

        $values['aboutperson'] = strip_tags($sql['aboutperson']);
        printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
    }
    include('form.php');

}else {

    $errors = FALSE;
    if (empty($_POST['name'])) {
        setcookie('name_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        $name = $_POST["name"];
        if (!preg_match("/^[a-zA-Zа-яА-Я]+$/ui", $name)) {
            $nameErr = "Only letters";
            setcookie('name_error', '1', time() + 24 * 60 * 60);
            $errors = TRUE;
        } else setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['email'])) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        $email = $_POST["email"];
        if (!preg_match("/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/u", $email)) {
            $email_error = "Invalid email format";
        } else
            setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['dateOfBirth'])) {
        setcookie('dateOfBirth_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        $dateOfBirth = $_POST["dateOfBirth"];
         if (!preg_match("/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/", $dateOfBirth)) {
             setcookie('dateOfBirth_error', $_POST['dateOfBirth'], time() + 30 * 24 * 60 * 60);
             $errors = TRUE;
         } else
        setcookie('dateOfBirth_value', $_POST['dateOfBirth'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['first_radio_group'])) {
        setcookie('sop_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        $sop = $_POST['first_radio_group'];
        if ($sop !='Мужской' && $sop!='Женский') {
            setcookie('sop_error', '1', time() + 30 * 24 * 60 * 60);
            $errors = TRUE;
        } else
            setcookie('sop_value', $_POST['first_radio_group'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['second_radio_group'])) {
        setcookie('col_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        $col = $_POST['second_radio_group'];
        if ($col != 'Меньше четырёх' && $col != 'Четыре' && $col != 'Больше четырёх') {
            setcookie('col_error', '1', time() + 30 * 24 * 60 * 60);
            $errors = TRUE;
        } else
            setcookie('col_value', $_POST['second_radio_group'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['superpower'])) {
        setcookie('superpower_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        $superpower = array();
        $superpower = $_POST['superpower'];
        $on[0] = 0;
        $on[1] = 0;
        $on[2] = 0;
        for ($key = 0; $key < count($superpower); $key++) {
            if ($superpower[$key] != 'Бессмертие' && $superpower[$key] != 'Прохождение сквозь стены' && $superpower[$key] != 'Левитация' ) {
                setcookie('superpower_error', '1', time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            } else{
                if($superpower[$key] == 'Бессмертие') {
                    setcookie("superpower_value[0]", "$superpower[$key]", time() + 30 * 24 * 60 * 60);
                    $on[0]=1;
                }else if($on[0]==0)setcookie("superpower_value[0]", '', 100000);

                if($superpower[$key] == 'Прохождение сквозь стены') {
                    setcookie("superpower_value[1]", "$superpower[$key]", time() + 30 * 24 * 60 * 60);
                    $on[1]=1;
                }else if($on[1]==0)setcookie("superpower_value[1]", '', 100000);

                if($superpower[$key] == 'Левитация') {
                    setcookie("superpower_value[2]", "$superpower[$key]", time() + 30 * 24 * 60 * 60);
                    $on[2]=1;
                }else if($on[2]==0) {
                    setcookie("superpower_value[2]", '', 100000);
                }
            }
        }
    }

    print_r($superpower);
    echo '<br>';
    print_r(count($superpower));
    echo '<br>';
    if (isset($_COOKIE['superpower_value'])) {
        foreach ($_COOKIE['superpower_value'] as $name => $value) {
            $name = htmlspecialchars($name);
            $value = htmlspecialchars($value);
            echo "$name : $value <br />\n";
        }
    }


    if (empty($_POST['contract'])) {
        setcookie('contract_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('contract_value', $_POST['contract'], time() + 30 * 24 * 60 * 60);
    }

    if ($errors) {
        header('Location: index.php');
        exit();
    } else {
        setcookie('name_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('dateOfBirth_error', '', 100000);
        setcookie('sop_error', '', 100000);
        setcookie('col_error', '', 100000);
        setcookie('superpower_error', '', 100000);
        setcookie('contract_error', '', 100000);
    }


    if (!empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
        // TODO: перезаписать данные в БД новыми данными,
        $data = array('name' => $_POST['name'], 'email' => $_POST['email'], 'dateofbirth' => $_POST['dateOfBirth'], 'sop' => $_POST['first_radio_group'], 'col' => $_POST['second_radio_group'], 'message' => $_POST['message']);
        $stmt = $db->prepare("UPDATE person SET name = :name, email = :email, dateofbirth = :dateofbirth, sop = :sop, col = :col, aboutperson = :aboutperson WHERE person.id = (SELECT user_id FROM users WHERE user_login = :ulogin)");
        $stmt->bindParam(':ulogin', $_SESSION['login']);
        $stmt->bindParam(':name', $_POST['name']);
        $stmt->bindParam(':email', $_POST['email']);
        $stmt->bindParam(':dateofbirth', $_POST['dateOfBirth']);
        $stmt->bindParam(':sop', $_POST['first_radio_group']);
        $stmt->bindParam(':col', $_POST['second_radio_group']);
        $stmt->bindParam(':aboutperson', $_POST['message']);
        $stmt->execute();


        $stmt = $db->prepare("SELECT user_id FROM users WHERE user_login = :login");
        $stmt->bindParam(':login', $_SESSION['login']);
        $stmt->execute();

        $ids = $stmt->fetch();
        $id = $ids[0];

        $stmt = $db->prepare("DELETE FROM superpowers WHERE person_id=:id ");
        $stmt->bindParam(':id', $id);
        $stmt->execute();


        $super = array();
        $super = $_POST['superpower'];
        $cos=0;

        foreach($super as $s) {
            $stmt = $db->prepare("INSERT INTO superpowers(person_id, superpower) VALUES (:id, :superpower)");
            $stmt->bindParam(':id', $id);
            $stmt->bindParam(':superpower', $super[$cos]);
            $stmt->execute();
            $cos++;
        }
    }
    else {
        function generate_login($len)
        {
            $r = "";
            $allowedSymbols = array("b", "a", "c", "e", "d", "o", "f", "i", "g", "u", "h", "o", "j", "e", "k", "o", "l", "e", "m", "u", "n", "o", "p", "a", "q", "e", "r", "o", "s", "i", "t", "u", "v", "e", "w", "a", "x", "o", "y", "i", "z");
            for ($i = 0; $i < $len; $i++) {
                $rand = array_rand($allowedSymbols);
                if ($i % 2 == 0) {
                    while ($rand % 2 != 0) {
                        $rand = array_rand($allowedSymbols);
                    }
                    $r .= $allowedSymbols[$rand];
                } else {
                    while ($rand % 2 == 0) {
                        $rand = array_rand($allowedSymbols);
                    }
                    $r .= $allowedSymbols[$rand];
                }
            }
            return $r;
        }
        function generate_password($number)
        {
            $arr = array('a', 'b', 'c', 'd', 'e', 'f',
                'g', 'h', 'i', 'j', 'k', 'l',
                'm', 'n', 'o', 'p', 'r', 's',
                't', 'u', 'v', 'x', 'y', 'z',
                'A', 'B', 'C', 'D', 'E', 'F',
                'G', 'H', 'I', 'J', 'K', 'L',
                'M', 'N', 'O', 'P', 'R', 'S',
                'T', 'U', 'V', 'X', 'Y', 'Z',
                '1', '2', '3', '4', '5', '6',
                '7', '8', '9', '0', '.', ',',
                '(', ')', '[', ']', '!', '?',
                '&', '^', '%', '@', '*', '$',
                '<', '>', '/', '|', '+', '-',
                '{', '}', '`', '~');
            $pass = "";
            for ($i = 0; $i < $number; $i++) {
                // Вычисляем случайный индекс массива
                $index = rand(0, count($arr) - 1);
                $pass .= $arr[$index];
            }
            return $pass;
        }

        $login = generate_login(5);
        $password = generate_password(8);
        $lpass = md5($password);

        setcookie('login', $login);
        setcookie('pass', $password);

        try {
            $uap = $db->prepare("INSERT INTO users (user_login, user_password) VALUES (:login, :pass)");
            $uap->bindParam(':login', $login);
            $uap->bindParam(':pass', $lpass);
            $uap->execute();

            $data = array('name' => $_POST['name'], 'email' => $_POST['email'], 'dateofbirth' => $_POST['dateOfBirth'], 'sop' => $_POST['first_radio_group'], 'col' => $_POST['second_radio_group'], 'message' => $_POST['message']);
            $stmt = $db->prepare("INSERT INTO person (name, email, dateofbirth, sop, col, aboutperson) VALUES (:name, :email, :dateofbirth, :sop, :col, :aboutperson)");
            $stmt->bindParam(':name', $_POST['name']);
            $stmt->bindParam(':email', $_POST['email']);
            $stmt->bindParam(':dateofbirth', $_POST['dateOfBirth']);
            $stmt->bindParam(':sop', $_POST['first_radio_group']);
            $stmt->bindParam(':col', $_POST['second_radio_group']);
            $stmt->bindParam(':aboutperson', $_POST['message']);
            $stmt->execute();

            $super = array();
            $super = $_POST['superpower'];
            $cos=0;

            $stmt = $db->prepare("SELECT user_id FROM users WHERE user_login = :login");
            $stmt->bindParam(':login', $login);
            $stmt->execute();
            $ids = $stmt->fetch();
            $id = $ids[0];

            foreach($super as $s) {
                $stmt = $db->prepare("INSERT INTO superpowers(person_id, superpower) VALUES (:id, :superpower)");
                $stmt->bindParam(':id', $id);
                $stmt->bindParam(':superpower', $super[$cos]);
                $stmt->execute();
                $cos++;
            }

        } catch (PDOException $e) {
            print('PDOError : ' . $e->getMessage());
            exit();
        }
    }
    setcookie('save', '1');
    header('Location: index.php');
}

