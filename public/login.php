<?php
include'pdoConfig.php';
header('Content-Type: text/html; charset=UTF-8');

session_start();

if (!empty($_SESSION['login'])) {
    if($_GET['do'] == 'logout'){
        unset($_SESSION['login']);
        session_destroy();
        header('Location: index.php');
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    include('loginStyle.html');

}
else {
    $login = $_POST['login'];
    $pass = $_POST['pass'];

    $result=0;
    $uid = 'error';
    try {
        $db = new PDO($dsn, $username, $dbPassword);
        $stmt = $db->prepare("SELECT user_password FROM users WHERE user_login = :login LIMIT 1");
        $stmt->bindParam(':login',  $_POST['login']);
        $stmt->execute();

        $result = $stmt->fetch();

        $stmt = $db->prepare("SELECT user_id FROM users WHERE user_login = :login LIMIT 1");
        $stmt->bindParam(':login',  $_POST['login']);
        $stmt->execute();

        $uid = $stmt->fetch();

    }catch (PDOException $e) {
        print('PDO:'. $e->getMessage());
    }

    if($result!=0)
    {
        $lpass = $result['user_password'];
    }
    else {
        $lpass = "none";
        $err[] = "Неверно имя пользователя или пароль";
    }

    if($lpass == md5($_POST['pass'])){
        $_SESSION['login'] = $_POST['login'];
        $_SESSION['pass'] = $_POST['pass'];
        $_SESSION['uid'] = $uid['user_id'];
        header("Location: index.php");
    }else {
        echo '<p>Логин или пароль неверны!</p>';
        echo '<a href="login.php">назад</a>';
    }
}


