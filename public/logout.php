<?php
$_SERVER['PHP_AUTH_USER']=0;
if(empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW'])) {
    Header("WWW-Authenticate: Basic realm=\"Admin Page\"");
    Header("HTTP/1.0 401 Unauthorized");
    print('<h1><a class="btf" href="index.php">Назад к форме</a></h1>');
}
?>
<style>
    .btf{
        text-decoration: none;
        font-family: Roboto, sans-serif;
        font-weight: bolder;
        color: #000000;
    }

    .btf:hover{
        color: #757575;
    }

</style>
