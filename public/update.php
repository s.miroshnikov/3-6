<?php
session_start();

$unique = $_SESSION['unique'];

include 'pdoConfig.php';

try {
    $db = new PDO($dsn, $username, $dbPassword);
} catch (PDOException $error) {
    echo 'Connection error:' . $error->getMessage();
}

try {
    $data = array('name' => $_POST['name'], 'email' => $_POST['email'], 'dateofbirth' => $_POST['dateOfBirth'], 'sop' => $_POST['first_radio_group'], 'col' => $_POST['second_radio_group'], 'message' => $_POST['message']);
    $stmt = $db->prepare("UPDATE person SET name = :name, email = :email, dateofbirth = :dateofbirth, sop = :sop, col = :col, aboutperson = :aboutperson WHERE person.id = '$unique'");
    $stmt->bindParam(':name', $_POST['name']);
    $stmt->bindParam(':email', $_POST['email']);
    $stmt->bindParam(':dateofbirth', $_POST['dateOfBirth']);
    $stmt->bindParam(':sop', $_POST['first_radio_group']);
    $stmt->bindParam(':col', $_POST['second_radio_group']);
    $stmt->bindParam(':aboutperson', $_POST['message']);
    $stmt->execute();


} catch (PDOException $e) {
    print('PDOError : ' . $e->getMessage());
    exit();
}
?>
<h1>Данные изменены.</h1>
<a id="toForm" href="index.php">К форме</a>
<a id="toExit" href="login.php?do=logout">Выход</a>
<style>
    *{
        margin: 0;
        padding: 0;
    }

    html{
        background-color: #ffffff;
        font-family:Roboto Light, sans-serif;
    }

    #toForm{
        text-decoration: none;
        font-weight: bolder;
        color:black;
    }

    #toForm:hover{
        color: #000000;
        text-shadow: 1px 1px 1px #404040;
    }

    #toExit{
        text-decoration: none;
        font-weight: bolder;
        color:black;
    }

    #toExit:hover{
        color: #000000;
        text-shadow: 1px 1px 1px #404040;
    }
</style>
